# Status LEDs Submodule

## Usage
D6 - D12 are the status LEDs that indicate volume. D6 being the lowest volume and D12 being the highest volume. Each LED indicates a 12.5% increase in volume over the LED below it. 

LED D3 indicates if the input audio jack is receiving a signal. When it is on it means a audio jack is connected and the piHAT is receiving a signal. If the LED is off the input audio jack is not receiving a signal. 

LED D4 indicates if a signal is being sent to the output audio jack. If the LED is on then a signal is being recieved at the output audio jack. If the LED is off the autput audio jack is not receiving a signal. 
