1. Download schematics and PCB designs

2. Run PCB error check to ensure there are no errors

3. Have PCB printed electronically using a PCB Printer using materials outlined in BOM below

BOM: https://gitlab.com/cllste009/eee3088f-pi-hat-project/-/blob/master/full-pcb/BOM.csv 
