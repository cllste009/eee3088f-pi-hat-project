EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5900 3550 550  900 
U 60C73CFD
F0 "sheet60C73CF4" 50
F1 "amplifier.sch" 50
F2 "IN" I L 5900 3900 50 
F3 "VP" I L 5900 4150 50 
F4 "VN" I L 5900 4350 50 
F5 "OUT" I R 6450 3850 50 
F6 "VOL_OUT" I R 6450 3750 50 
$EndSheet
$Sheet
S 4300 3850 850  750 
U 60C73D01
F0 "sheet60C73CF5" 50
F1 "power.sch" 50
F2 "VP" I R 5150 4150 50 
F3 "VN" I R 5150 4350 50 
F4 "+5V" I L 4300 4250 50 
$EndSheet
Wire Wire Line
	5150 4150 5900 4150
Wire Wire Line
	5150 4350 5900 4350
$Sheet
S 6700 2750 600  500 
U 60C73D08
F0 "sheet60C73CF6" 50
F1 "leds.sch" 50
F2 "VOL_OUT" I L 6700 3000 50 
F3 "IN" I L 6700 2900 50 
F4 "OUT" I L 6700 3100 50 
$EndSheet
Wire Wire Line
	5900 3900 5750 3900
Wire Wire Line
	5750 2900 6700 2900
Wire Wire Line
	5750 2900 5750 3400
Wire Wire Line
	6450 3750 6600 3750
Wire Wire Line
	6600 3750 6600 3000
Wire Wire Line
	6600 3000 6700 3000
Wire Wire Line
	6450 3850 6650 3850
Wire Wire Line
	6650 3850 6650 3100
Wire Wire Line
	6650 3100 6700 3100
Text Label 5350 3400 2    50   ~ 0
AUDIO_IN
Wire Wire Line
	5350 3400 5750 3400
Connection ~ 5750 3400
Wire Wire Line
	5750 3400 5750 3900
$Sheet
S 2800 3950 950  500 
U 60C383CA
F0 "Pi HAT" 50
F1 "pi-hat.sch" 50
F2 "+3.3V" I R 3750 4100 50 
F3 "+5V" I R 3750 4250 50 
$EndSheet
Wire Wire Line
	3750 4250 4300 4250
NoConn ~ 3750 4100
$EndSCHEMATC
