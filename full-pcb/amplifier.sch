EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:ADA4807-4 UH1
U 1 1 60BA30AB
P 4250 4500
AR Path="/60C37703/60C39C85/60BA30AB" Ref="UH1"  Part="1" 
AR Path="/60C73CFD/60BA30AB" Ref="UH?"  Part="1" 
F 0 "UH?" H 4250 4133 50  0000 C CNN
F 1 "ADA4807-4" H 4250 4224 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 4200 4600 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 4300 4700 50  0001 C CNN
	1    4250 4500
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UH1
U 2 1 60BA6A0B
P 6150 4400
AR Path="/60C37703/60C39C85/60BA6A0B" Ref="UH1"  Part="2" 
AR Path="/60C73CFD/60BA6A0B" Ref="UH?"  Part="2" 
F 0 "UH?" H 6150 4767 50  0000 C CNN
F 1 "ADA4807-4" H 6150 4676 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 6100 4500 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 6200 4600 50  0001 C CNN
	2    6150 4400
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UH1
U 4 1 60BAB04F
P 9950 4200
AR Path="/60C37703/60C39C85/60BAB04F" Ref="UH1"  Part="4" 
AR Path="/60C73CFD/60BAB04F" Ref="UH?"  Part="4" 
F 0 "UH?" H 9950 4567 50  0000 C CNN
F 1 "ADA4807-4" H 9950 4476 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 9900 4300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 10000 4400 50  0001 C CNN
	4    9950 4200
	1    0    0    1   
$EndComp
$Comp
L pspice:CAP C2
U 1 1 60BC0939
P 4650 2350
AR Path="/60C37703/60C39C85/60BC0939" Ref="C2"  Part="1" 
AR Path="/60C73CFD/60BC0939" Ref="C?"  Part="1" 
F 0 "C?" H 4828 2396 50  0000 L CNN
F 1 "1.5n" H 4828 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4650 2350 50  0001 C CNN
F 3 "~" H 4650 2350 50  0001 C CNN
	1    4650 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 60BC2205
P 4300 1950
AR Path="/60C37703/60C39C85/60BC2205" Ref="R5"  Part="1" 
AR Path="/60C73CFD/60BC2205" Ref="R?"  Part="1" 
F 0 "R?" V 4505 1950 50  0000 C CNN
F 1 "100k" V 4414 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4340 1940 50  0001 C CNN
F 3 "~" H 4300 1950 50  0001 C CNN
	1    4300 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 1950 4650 1950
Wire Wire Line
	4650 2100 4650 1950
Connection ~ 4650 1950
Wire Wire Line
	4650 1950 4850 1950
Wire Wire Line
	3950 4400 3800 4400
Wire Wire Line
	3800 4400 3800 4000
Wire Wire Line
	3800 4000 4550 4000
Wire Wire Line
	4550 4000 4550 4500
$Comp
L Device:R_US R6
U 1 1 60BE9305
P 5350 4950
AR Path="/60C37703/60C39C85/60BE9305" Ref="R6"  Part="1" 
AR Path="/60C73CFD/60BE9305" Ref="R?"  Part="1" 
F 0 "R?" V 5555 4950 50  0000 C CNN
F 1 "22k" V 5464 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5390 4940 50  0001 C CNN
F 3 "~" H 5350 4950 50  0001 C CNN
	1    5350 4950
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C3
U 1 1 60BE25EA
P 4800 4500
AR Path="/60C37703/60C39C85/60BE25EA" Ref="C3"  Part="1" 
AR Path="/60C73CFD/60BE25EA" Ref="C?"  Part="1" 
F 0 "C?" V 4485 4500 50  0000 C CNN
F 1 "1.5n" V 4576 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4800 4500 50  0001 C CNN
F 3 "~" H 4800 4500 50  0001 C CNN
	1    4800 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R8
U 1 1 60BCDE68
P 5650 4650
AR Path="/60C37703/60C39C85/60BCDE68" Ref="R8"  Part="1" 
AR Path="/60C73CFD/60BCDE68" Ref="R?"  Part="1" 
F 0 "R?" H 5718 4696 50  0000 L CNN
F 1 "470k" H 5718 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5690 4640 50  0001 C CNN
F 3 "~" H 5650 4650 50  0001 C CNN
	1    5650 4650
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C4
U 1 1 60BCDE62
P 5300 4500
AR Path="/60C37703/60C39C85/60BCDE62" Ref="C4"  Part="1" 
AR Path="/60C73CFD/60BCDE62" Ref="C?"  Part="1" 
F 0 "C?" V 4985 4500 50  0000 C CNN
F 1 "1.5n" V 5076 4500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5300 4500 50  0001 C CNN
F 3 "~" H 5300 4500 50  0001 C CNN
	1    5300 4500
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UH1
U 3 1 60BA96CC
P 8050 4300
AR Path="/60C37703/60C39C85/60BA96CC" Ref="UH1"  Part="3" 
AR Path="/60C73CFD/60BA96CC" Ref="UH?"  Part="3" 
F 0 "UH?" H 8050 4667 50  0000 C CNN
F 1 "ADA4807-4" H 8050 4576 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 8000 4400 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 8100 4500 50  0001 C CNN
	3    8050 4300
	1    0    0    1   
$EndComp
Connection ~ 4550 4500
$Comp
L Amplifier_Operational:ADA4807-4 UH1
U 5 1 60BADCFB
P 1000 1150
AR Path="/60C37703/60C39C85/60BADCFB" Ref="UH1"  Part="5" 
AR Path="/60C73CFD/60BADCFB" Ref="UH?"  Part="5" 
F 0 "UH?" H 958 1196 50  0000 L CNN
F 1 "ADA4807-4" H 958 1105 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 950 1250 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 1050 1350 50  0001 C CNN
	5    1000 1150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UL1
U 1 1 60C83DCA
P 5150 1850
AR Path="/60C37703/60C39C85/60C83DCA" Ref="UL1"  Part="1" 
AR Path="/60C73CFD/60C83DCA" Ref="UL?"  Part="1" 
F 0 "UL?" H 5150 1483 50  0000 C CNN
F 1 "ADA4807-4" H 5150 1574 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 5100 1950 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 5200 2050 50  0001 C CNN
	1    5150 1850
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UL1
U 2 1 60C85AE0
P 6700 1750
AR Path="/60C37703/60C39C85/60C85AE0" Ref="UL1"  Part="2" 
AR Path="/60C73CFD/60C85AE0" Ref="UL?"  Part="2" 
F 0 "UL?" H 6700 1383 50  0000 C CNN
F 1 "ADA4807-4" H 6700 1474 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 6650 1850 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 6750 1950 50  0001 C CNN
	2    6700 1750
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UL1
U 3 1 60C8A08B
P 8250 1650
AR Path="/60C37703/60C39C85/60C8A08B" Ref="UL1"  Part="3" 
AR Path="/60C73CFD/60C8A08B" Ref="UL?"  Part="3" 
F 0 "UL?" H 8250 1283 50  0000 C CNN
F 1 "ADA4807-4" H 8250 1374 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 8200 1750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 8300 1850 50  0001 C CNN
	3    8250 1650
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UL1
U 4 1 60C8C23F
P 9800 1550
AR Path="/60C37703/60C39C85/60C8C23F" Ref="UL1"  Part="4" 
AR Path="/60C73CFD/60C8C23F" Ref="UL?"  Part="4" 
F 0 "UL?" H 9800 1183 50  0000 C CNN
F 1 "ADA4807-4" H 9800 1274 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 9750 1650 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 9850 1750 50  0001 C CNN
	4    9800 1550
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:ADA4807-4 UL1
U 5 1 60C8E439
P 1750 1150
AR Path="/60C37703/60C39C85/60C8E439" Ref="UL1"  Part="5" 
AR Path="/60C73CFD/60C8E439" Ref="UL?"  Part="5" 
F 0 "UL?" H 1708 1196 50  0000 L CNN
F 1 "ADA4807-4" H 1708 1105 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1700 1250 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 1800 1350 50  0001 C CNN
	5    1750 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1750 4850 1500
Wire Wire Line
	4850 1500 5450 1500
Wire Wire Line
	5450 1500 5450 1850
$Comp
L Device:R_US R7
U 1 1 60CB4130
P 5600 1850
AR Path="/60C37703/60C39C85/60CB4130" Ref="R7"  Part="1" 
AR Path="/60C73CFD/60CB4130" Ref="R?"  Part="1" 
F 0 "R?" V 5805 1850 50  0000 C CNN
F 1 "470k" V 5714 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5640 1840 50  0001 C CNN
F 3 "~" H 5600 1850 50  0001 C CNN
	1    5600 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R9
U 1 1 60CB4FB8
P 6000 1850
AR Path="/60C37703/60C39C85/60CB4FB8" Ref="R9"  Part="1" 
AR Path="/60C73CFD/60CB4FB8" Ref="R?"  Part="1" 
F 0 "R?" V 6205 1850 50  0000 C CNN
F 1 "470k" V 6114 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6040 1840 50  0001 C CNN
F 3 "~" H 6000 1850 50  0001 C CNN
	1    6000 1850
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C6
U 1 1 60CBB1E0
P 6300 2100
AR Path="/60C37703/60C39C85/60CBB1E0" Ref="C6"  Part="1" 
AR Path="/60C73CFD/60CBB1E0" Ref="C?"  Part="1" 
F 0 "C?" H 6478 2146 50  0000 L CNN
F 1 "82p" H 6478 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6300 2100 50  0001 C CNN
F 3 "~" H 6300 2100 50  0001 C CNN
	1    6300 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1850 6300 1850
Wire Wire Line
	5750 1850 5800 1850
Connection ~ 6300 1850
Wire Wire Line
	6300 1850 6400 1850
$Comp
L pspice:CAP C5
U 1 1 60CC0B8A
P 6050 2550
AR Path="/60C37703/60C39C85/60CC0B8A" Ref="C5"  Part="1" 
AR Path="/60C73CFD/60CC0B8A" Ref="C?"  Part="1" 
F 0 "C?" V 6365 2550 50  0000 C CNN
F 1 "1.5n" V 6274 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6050 2550 50  0001 C CNN
F 3 "~" H 6050 2550 50  0001 C CNN
	1    6050 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 1850 5800 2550
Connection ~ 5800 1850
Wire Wire Line
	5800 1850 5850 1850
Wire Wire Line
	7000 2550 7000 1750
Wire Wire Line
	6350 1650 6350 1350
Wire Wire Line
	7000 1350 7000 1750
Connection ~ 7000 1750
Wire Wire Line
	6300 2550 7000 2550
Wire Wire Line
	6350 1350 7000 1350
Wire Wire Line
	6350 1650 6400 1650
$Comp
L Device:R_US R10
U 1 1 60CDD65A
P 7150 1750
AR Path="/60C37703/60C39C85/60CDD65A" Ref="R10"  Part="1" 
AR Path="/60C73CFD/60CDD65A" Ref="R?"  Part="1" 
F 0 "R?" V 7355 1750 50  0000 C CNN
F 1 "82k" V 7264 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7190 1740 50  0001 C CNN
F 3 "~" H 7150 1750 50  0001 C CNN
	1    7150 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R12
U 1 1 60CDD660
P 7550 1750
AR Path="/60C37703/60C39C85/60CDD660" Ref="R12"  Part="1" 
AR Path="/60C73CFD/60CDD660" Ref="R?"  Part="1" 
F 0 "R?" V 7755 1750 50  0000 C CNN
F 1 "1.2M" V 7664 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7590 1740 50  0001 C CNN
F 3 "~" H 7550 1750 50  0001 C CNN
	1    7550 1750
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C10
U 1 1 60CDD666
P 7850 2000
AR Path="/60C37703/60C39C85/60CDD666" Ref="C10"  Part="1" 
AR Path="/60C73CFD/60CDD666" Ref="C?"  Part="1" 
F 0 "C?" H 8028 2046 50  0000 L CNN
F 1 "150p" H 8028 1955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7850 2000 50  0001 C CNN
F 3 "~" H 7850 2000 50  0001 C CNN
	1    7850 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1750 7850 1750
Wire Wire Line
	7300 1750 7350 1750
Connection ~ 7850 1750
Wire Wire Line
	7850 1750 7950 1750
$Comp
L pspice:CAP C9
U 1 1 60CDD670
P 7600 2450
AR Path="/60C37703/60C39C85/60CDD670" Ref="C9"  Part="1" 
AR Path="/60C73CFD/60CDD670" Ref="C?"  Part="1" 
F 0 "C?" V 7915 2450 50  0000 C CNN
F 1 "1.5n" V 7824 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7600 2450 50  0001 C CNN
F 3 "~" H 7600 2450 50  0001 C CNN
	1    7600 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 1750 7350 2450
Connection ~ 7350 1750
Wire Wire Line
	7350 1750 7400 1750
Wire Wire Line
	7900 1550 7900 1250
Wire Wire Line
	7850 2450 8550 2450
Wire Wire Line
	7900 1250 8550 1250
Wire Wire Line
	7900 1550 7950 1550
$Comp
L Device:R_US R14
U 1 1 60CE6243
P 8700 1650
AR Path="/60C37703/60C39C85/60CE6243" Ref="R14"  Part="1" 
AR Path="/60C73CFD/60CE6243" Ref="R?"  Part="1" 
F 0 "R?" V 8905 1650 50  0000 C CNN
F 1 "56k" V 8814 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8740 1640 50  0001 C CNN
F 3 "~" H 8700 1650 50  0001 C CNN
	1    8700 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R15
U 1 1 60CE6249
P 9100 1650
AR Path="/60C37703/60C39C85/60CE6249" Ref="R15"  Part="1" 
AR Path="/60C73CFD/60CE6249" Ref="R?"  Part="1" 
F 0 "R?" V 9305 1650 50  0000 C CNN
F 1 "1.8M" V 9214 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9140 1640 50  0001 C CNN
F 3 "~" H 9100 1650 50  0001 C CNN
	1    9100 1650
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C14
U 1 1 60CE624F
P 9400 1900
AR Path="/60C37703/60C39C85/60CE624F" Ref="C14"  Part="1" 
AR Path="/60C73CFD/60CE624F" Ref="C?"  Part="1" 
F 0 "C?" H 9578 1946 50  0000 L CNN
F 1 "150p" H 9578 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9400 1900 50  0001 C CNN
F 3 "~" H 9400 1900 50  0001 C CNN
	1    9400 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1650 9400 1650
Wire Wire Line
	8850 1650 8900 1650
Connection ~ 9400 1650
Wire Wire Line
	9400 1650 9500 1650
$Comp
L pspice:CAP C13
U 1 1 60CE6259
P 9150 2350
AR Path="/60C37703/60C39C85/60CE6259" Ref="C13"  Part="1" 
AR Path="/60C73CFD/60CE6259" Ref="C?"  Part="1" 
F 0 "C?" V 9465 2350 50  0000 C CNN
F 1 "1.5n" V 9374 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9150 2350 50  0001 C CNN
F 3 "~" H 9150 2350 50  0001 C CNN
	1    9150 2350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 1650 8900 2350
Connection ~ 8900 1650
Wire Wire Line
	8900 1650 8950 1650
Wire Wire Line
	9450 1450 9450 1150
Wire Wire Line
	9400 2350 10100 2350
Wire Wire Line
	9450 1150 10100 1150
Wire Wire Line
	9450 1450 9500 1450
Wire Wire Line
	10100 1150 10100 1550
Wire Wire Line
	10100 2350 10100 1550
Connection ~ 10100 1550
Wire Wire Line
	8550 1250 8550 1650
Wire Wire Line
	8550 1650 8550 2450
Connection ~ 8550 1650
Connection ~ 5450 1850
Wire Wire Line
	3950 4800 3950 4600
Wire Wire Line
	3800 4600 3950 4600
Connection ~ 3950 4600
Wire Wire Line
	5550 4500 5650 4500
Wire Wire Line
	5850 4500 5650 4500
Connection ~ 5650 4500
Wire Wire Line
	5050 4500 5050 4950
Wire Wire Line
	5050 4950 5200 4950
Connection ~ 5050 4500
Wire Wire Line
	5500 4950 6450 4950
Wire Wire Line
	6450 4950 6450 4400
Wire Wire Line
	6450 4100 6450 4400
Connection ~ 6450 4400
Wire Wire Line
	5850 4300 5800 4300
Wire Wire Line
	5800 4300 5800 4100
Wire Wire Line
	5800 4100 6450 4100
$Comp
L Device:R_US R11
U 1 1 60D5F6F2
P 7250 4850
AR Path="/60C37703/60C39C85/60D5F6F2" Ref="R11"  Part="1" 
AR Path="/60C73CFD/60D5F6F2" Ref="R?"  Part="1" 
F 0 "R?" V 7455 4850 50  0000 C CNN
F 1 "68k" V 7364 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7290 4840 50  0001 C CNN
F 3 "~" H 7250 4850 50  0001 C CNN
	1    7250 4850
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C7
U 1 1 60D5F6F8
P 6700 4400
AR Path="/60C37703/60C39C85/60D5F6F8" Ref="C7"  Part="1" 
AR Path="/60C73CFD/60D5F6F8" Ref="C?"  Part="1" 
F 0 "C?" V 6385 4400 50  0000 C CNN
F 1 "1.5n" V 6476 4400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6700 4400 50  0001 C CNN
F 3 "~" H 6700 4400 50  0001 C CNN
	1    6700 4400
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R13
U 1 1 60D5F6FE
P 7550 4550
AR Path="/60C37703/60C39C85/60D5F6FE" Ref="R13"  Part="1" 
AR Path="/60C73CFD/60D5F6FE" Ref="R?"  Part="1" 
F 0 "R?" H 7618 4596 50  0000 L CNN
F 1 "150k" H 7618 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7590 4540 50  0001 C CNN
F 3 "~" H 7550 4550 50  0001 C CNN
	1    7550 4550
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C8
U 1 1 60D5F704
P 7200 4400
AR Path="/60C37703/60C39C85/60D5F704" Ref="C8"  Part="1" 
AR Path="/60C73CFD/60D5F704" Ref="C?"  Part="1" 
F 0 "C?" V 6885 4400 50  0000 C CNN
F 1 "1.5n" V 6976 4400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7200 4400 50  0001 C CNN
F 3 "~" H 7200 4400 50  0001 C CNN
	1    7200 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 4400 7550 4400
Wire Wire Line
	7750 4400 7550 4400
Connection ~ 7550 4400
Wire Wire Line
	6950 4400 6950 4850
Wire Wire Line
	6950 4850 7100 4850
Connection ~ 6950 4400
Wire Wire Line
	7400 4850 8350 4850
Wire Wire Line
	7750 4200 7700 4200
Wire Wire Line
	7700 4200 7700 4000
Wire Wire Line
	7700 4000 8350 4000
Wire Wire Line
	8350 4000 8350 4300
$Comp
L Device:R_US R16
U 1 1 60D70B4E
P 9150 4750
AR Path="/60C37703/60C39C85/60D70B4E" Ref="R16"  Part="1" 
AR Path="/60C73CFD/60D70B4E" Ref="R?"  Part="1" 
F 0 "R?" V 9355 4750 50  0000 C CNN
F 1 "82k" V 9264 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9190 4740 50  0001 C CNN
F 3 "~" H 9150 4750 50  0001 C CNN
	1    9150 4750
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C11
U 1 1 60D70B54
P 8600 4300
AR Path="/60C37703/60C39C85/60D70B54" Ref="C11"  Part="1" 
AR Path="/60C73CFD/60D70B54" Ref="C?"  Part="1" 
F 0 "C?" V 8285 4300 50  0000 C CNN
F 1 "1.5n" V 8376 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8600 4300 50  0001 C CNN
F 3 "~" H 8600 4300 50  0001 C CNN
	1    8600 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R17
U 1 1 60D70B5A
P 9450 4450
AR Path="/60C37703/60C39C85/60D70B5A" Ref="R17"  Part="1" 
AR Path="/60C73CFD/60D70B5A" Ref="R?"  Part="1" 
F 0 "R?" H 9518 4496 50  0000 L CNN
F 1 "120k" H 9518 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9490 4440 50  0001 C CNN
F 3 "~" H 9450 4450 50  0001 C CNN
	1    9450 4450
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C12
U 1 1 60D70B60
P 9100 4300
AR Path="/60C37703/60C39C85/60D70B60" Ref="C12"  Part="1" 
AR Path="/60C73CFD/60D70B60" Ref="C?"  Part="1" 
F 0 "C?" V 8785 4300 50  0000 C CNN
F 1 "1.5n" V 8876 4300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9100 4300 50  0001 C CNN
F 3 "~" H 9100 4300 50  0001 C CNN
	1    9100 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 4300 9450 4300
Wire Wire Line
	9650 4300 9450 4300
Connection ~ 9450 4300
Wire Wire Line
	8850 4300 8850 4750
Wire Wire Line
	8850 4750 9000 4750
Connection ~ 8850 4300
Wire Wire Line
	9300 4750 10250 4750
Wire Wire Line
	9650 4100 9600 4100
Wire Wire Line
	9600 4100 9600 3900
Wire Wire Line
	9600 3900 10250 3900
Wire Wire Line
	10250 3900 10250 4200
Wire Wire Line
	10250 4750 10250 4200
Connection ~ 10250 4200
Connection ~ 8350 4300
Wire Wire Line
	8350 4850 8350 4300
$Comp
L power:GND #PWR0101
U 1 1 60DF6DE0
P 3950 5100
AR Path="/60C37703/60C39C85/60DF6DE0" Ref="#PWR0101"  Part="1" 
AR Path="/60C73CFD/60DF6DE0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 4850 50  0001 C CNN
F 1 "GND" H 3955 4927 50  0000 C CNN
F 2 "" H 3950 5100 50  0001 C CNN
F 3 "" H 3950 5100 50  0001 C CNN
	1    3950 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60DFD91E
P 7550 4700
AR Path="/60C37703/60C39C85/60DFD91E" Ref="#PWR0102"  Part="1" 
AR Path="/60C73CFD/60DFD91E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7550 4450 50  0001 C CNN
F 1 "GND" H 7555 4527 50  0000 C CNN
F 2 "" H 7550 4700 50  0001 C CNN
F 3 "" H 7550 4700 50  0001 C CNN
	1    7550 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60E01B32
P 5650 4800
AR Path="/60C37703/60C39C85/60E01B32" Ref="#PWR0103"  Part="1" 
AR Path="/60C73CFD/60E01B32" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5650 4550 50  0001 C CNN
F 1 "GND" H 5655 4627 50  0000 C CNN
F 2 "" H 5650 4800 50  0001 C CNN
F 3 "" H 5650 4800 50  0001 C CNN
	1    5650 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 60E0373D
P 9450 4600
AR Path="/60C37703/60C39C85/60E0373D" Ref="#PWR0104"  Part="1" 
AR Path="/60C73CFD/60E0373D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9450 4350 50  0001 C CNN
F 1 "GND" H 9455 4427 50  0000 C CNN
F 2 "" H 9450 4600 50  0001 C CNN
F 3 "" H 9450 4600 50  0001 C CNN
	1    9450 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 60E05491
P 6300 2350
AR Path="/60C37703/60C39C85/60E05491" Ref="#PWR0105"  Part="1" 
AR Path="/60C73CFD/60E05491" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6300 2100 50  0001 C CNN
F 1 "GND" H 6305 2177 50  0000 C CNN
F 2 "" H 6300 2350 50  0001 C CNN
F 3 "" H 6300 2350 50  0001 C CNN
	1    6300 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 60E07272
P 7850 2250
AR Path="/60C37703/60C39C85/60E07272" Ref="#PWR0106"  Part="1" 
AR Path="/60C73CFD/60E07272" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7850 2000 50  0001 C CNN
F 1 "GND" H 7855 2077 50  0000 C CNN
F 2 "" H 7850 2250 50  0001 C CNN
F 3 "" H 7850 2250 50  0001 C CNN
	1    7850 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 60E08EF8
P 9400 2150
AR Path="/60C37703/60C39C85/60E08EF8" Ref="#PWR0107"  Part="1" 
AR Path="/60C73CFD/60E08EF8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9400 1900 50  0001 C CNN
F 1 "GND" H 9405 1977 50  0000 C CNN
F 2 "" H 9400 2150 50  0001 C CNN
F 3 "" H 9400 2150 50  0001 C CNN
	1    9400 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 60E0AD7A
P 4650 2600
AR Path="/60C37703/60C39C85/60E0AD7A" Ref="#PWR0108"  Part="1" 
AR Path="/60C73CFD/60E0AD7A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4650 2350 50  0001 C CNN
F 1 "GND" H 4655 2427 50  0000 C CNN
F 2 "" H 4650 2600 50  0001 C CNN
F 3 "" H 4650 2600 50  0001 C CNN
	1    4650 2600
	1    0    0    -1  
$EndComp
Text Label 900  1450 0    50   ~ 0
VN
Text Label 1650 850  0    50   ~ 0
VP
Text Label 900  850  0    50   ~ 0
VP
Text Label 4150 1950 2    50   ~ 0
VOL_OUT
Text Label 3300 4600 2    50   ~ 0
VOL_OUT
Text Label 1650 1450 0    50   ~ 0
VN
NoConn ~ 1850 6600
$Comp
L power:GND #PWR0109
U 1 1 60DF5D7E
P 2150 7000
AR Path="/60C37703/60C39C85/60DF5D7E" Ref="#PWR0109"  Part="1" 
AR Path="/60C73CFD/60DF5D7E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2150 6750 50  0001 C CNN
F 1 "GND" H 2155 6827 50  0000 C CNN
F 2 "" H 2150 7000 50  0001 C CNN
F 3 "" H 2150 7000 50  0001 C CNN
	1    2150 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6650 1550 6400
$Comp
L Device:R_US R1
U 1 1 60DEA7F0
P 2150 6850
AR Path="/60C37703/60C39C85/60DEA7F0" Ref="R1"  Part="1" 
AR Path="/60C73CFD/60DEA7F0" Ref="R?"  Part="1" 
F 0 "R?" H 2218 6896 50  0000 L CNN
F 1 "10k" H 2218 6805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2190 6840 50  0001 C CNN
F 3 "~" H 2150 6850 50  0001 C CNN
	1    2150 6850
	1    0    0    -1  
$EndComp
Text GLabel 1350 6200 0    50   Input ~ 0
IN
Wire Wire Line
	1350 6200 1550 6200
Text Label 2450 6300 0    50   ~ 0
VOL_OUT
Wire Wire Line
	2150 6300 2450 6300
$Comp
L Device:R_US R3
U 1 1 60E8B67B
P 4000 6300
AR Path="/60C37703/60C39C85/60E8B67B" Ref="R3"  Part="1" 
AR Path="/60C73CFD/60E8B67B" Ref="R?"  Part="1" 
F 0 "R?" V 4205 6300 50  0000 C CNN
F 1 "10k" V 4114 6300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4040 6290 50  0001 C CNN
F 3 "~" H 4000 6300 50  0001 C CNN
	1    4000 6300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R4
U 1 1 60E8D7EA
P 4000 6800
AR Path="/60C37703/60C39C85/60E8D7EA" Ref="R4"  Part="1" 
AR Path="/60C73CFD/60E8D7EA" Ref="R?"  Part="1" 
F 0 "R?" V 4205 6800 50  0000 C CNN
F 1 "9.1k" V 4114 6800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4040 6790 50  0001 C CNN
F 3 "~" H 4000 6800 50  0001 C CNN
	1    4000 6800
	0    -1   -1   0   
$EndComp
Text GLabel 5750 6550 2    50   Input ~ 0
OUT
Wire Wire Line
	5500 6550 5750 6550
$Comp
L Device:R_POT_Dual RV2
U 1 1 60E93560
P 4450 6550
AR Path="/60C37703/60C39C85/60E93560" Ref="RV2"  Part="1" 
AR Path="/60C73CFD/60E93560" Ref="RV?"  Part="1" 
F 0 "RV?" V 4496 6362 50  0000 R CNN
F 1 "100k" V 4405 6362 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3214W_Vertical" H 4700 6475 50  0001 C CNN
F 3 "~" H 4700 6475 50  0001 C CNN
	1    4450 6550
	0    -1   -1   0   
$EndComp
NoConn ~ 4550 6150
Wire Wire Line
	4150 6300 4350 6300
Wire Wire Line
	4150 6800 4350 6800
Wire Wire Line
	4550 6450 4900 6450
NoConn ~ 4550 6950
Text Label 10350 1550 0    50   ~ 0
LPF_OUT
Wire Wire Line
	10100 1550 10350 1550
Text Label 10500 4200 0    50   ~ 0
HPF_OUT
Wire Wire Line
	10250 4200 10500 4200
Text Label 3850 6300 2    50   ~ 0
LPF_OUT
Text Label 3850 6800 2    50   ~ 0
HPF_OUT
$Comp
L Device:R_US R2
U 1 1 60D0AC03
P 3950 4950
AR Path="/60C37703/60C39C85/60D0AC03" Ref="R2"  Part="1" 
AR Path="/60C73CFD/60D0AC03" Ref="R?"  Part="1" 
F 0 "R?" H 3883 4904 50  0000 R CNN
F 1 "100k" H 3883 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3990 4940 50  0001 C CNN
F 3 "~" H 3950 4950 50  0001 C CNN
	1    3950 4950
	1    0    0    1   
$EndComp
$Comp
L pspice:CAP C1
U 1 1 60D0ABFD
P 3550 4600
AR Path="/60C37703/60C39C85/60D0ABFD" Ref="C1"  Part="1" 
AR Path="/60C73CFD/60D0ABFD" Ref="C?"  Part="1" 
F 0 "C?" V 3235 4600 50  0000 C CNN
F 1 "1.5n" V 3326 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3550 4600 50  0001 C CNN
F 3 "~" H 3550 4600 50  0001 C CNN
	1    3550 4600
	0    1    -1   0   
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 60F3DD52
P 2150 6500
AR Path="/60C37703/60C39C85/60F3DD52" Ref="RV1"  Part="1" 
AR Path="/60C73CFD/60F3DD52" Ref="RV?"  Part="1" 
F 0 "RV?" H 2080 6546 50  0000 R CNN
F 1 "10k" H 2080 6455 50  0000 R CNN
F 2 "Potentiometer_SMD:Potentiometer_Bourns_3214W_Vertical" H 2150 6500 50  0001 C CNN
F 3 "~" H 2150 6500 50  0001 C CNN
	1    2150 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 6350 2150 6300
Wire Wire Line
	2150 6700 2150 6650
Wire Wire Line
	1550 6650 2150 6650
Connection ~ 2150 6650
$Comp
L Amplifier_Operational:ADA4807-1 U2
U 1 1 60FB2EFD
P 1850 6300
AR Path="/60C37703/60C39C85/60FB2EFD" Ref="U2"  Part="1" 
AR Path="/60C73CFD/60FB2EFD" Ref="U?"  Part="1" 
F 0 "U?" H 2194 6346 50  0000 L CNN
F 1 "ADA4807-1" H 2194 6255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 1850 6050 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 1850 5950 50  0001 L CNN
	1    1850 6300
	1    0    0    -1  
$EndComp
Connection ~ 2150 6300
$Comp
L Amplifier_Operational:ADA4807-1 U1
U 1 1 60C1A09C
P 5200 6550
AR Path="/60C37703/60C39C85/60C1A09C" Ref="U1"  Part="1" 
AR Path="/60C73CFD/60C1A09C" Ref="U?"  Part="1" 
F 0 "U?" H 5544 6596 50  0000 L CNN
F 1 "ADA4807-1" H 5544 6505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 5200 6300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4807-1_4807-2_4807-4.pdf" H 5200 6200 50  0001 L CNN
	1    5200 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 6650 4550 6450
Connection ~ 4550 6450
$Comp
L power:GND #PWR0110
U 1 1 60C22FE4
P 4900 6650
AR Path="/60C37703/60C39C85/60C22FE4" Ref="#PWR0110"  Part="1" 
AR Path="/60C73CFD/60C22FE4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4900 6400 50  0001 C CNN
F 1 "GND" H 4905 6477 50  0000 C CNN
F 2 "" H 4900 6650 50  0001 C CNN
F 3 "" H 4900 6650 50  0001 C CNN
	1    4900 6650
	1    0    0    -1  
$EndComp
Text Label 1750 6000 0    50   ~ 0
VP
Text Label 1750 6600 3    50   ~ 0
VN
Text Label 5100 6250 0    50   ~ 0
VP
Text Label 5100 6850 3    50   ~ 0
VN
NoConn ~ 5200 6850
Text Notes 7350 7500 0    50   ~ 0
Low/High Frequency Amplifier
Text Notes 10650 7650 0    50   ~ 0
1
Text Notes 8150 7650 0    50   ~ 0
11/06/2021\n
Text HLabel 1350 6200 1    50   Input ~ 0
IN
Text HLabel 900  1850 0    50   Input ~ 0
VP
Text HLabel 900  2050 0    50   Input ~ 0
VN
Text Label 1250 1850 0    50   ~ 0
VP
Wire Wire Line
	900  1850 1250 1850
Text Label 1250 2050 0    50   ~ 0
VN
Wire Wire Line
	900  2050 1250 2050
Text HLabel 5750 6550 1    50   Input ~ 0
OUT
Text HLabel 2400 6300 1    50   Input ~ 0
VOL_OUT
$EndSCHEMATC
