# Amplifier Submodule

## Usage

Connect +5V to the VP pin and -5V to VN.

Connect audio signal to the audio input Vin.

Attenuate volume, low, and high frequency using the potentiometers.
