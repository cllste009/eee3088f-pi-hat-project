Version 4
SymbolType BLOCK
RECTANGLE Normal -64 -72 80 72
TEXT 102 -37 Left 2 LPF
WINDOW 0 8 -72 Bottom 2
PIN -64 -48 LEFT 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN -64 -16 LEFT 8
PINATTR PinName Vn
PINATTR SpiceOrder 2
PIN -64 16 LEFT 8
PINATTR PinName Vp
PINATTR SpiceOrder 3
PIN -64 48 LEFT 8
PINATTR PinName Vref
PINATTR SpiceOrder 4
PIN 80 0 RIGHT 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
