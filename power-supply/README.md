# Power Supply Submodule

## Usage

Connect +5V to input voltage Vin

Connect output voltages, V+rail and V-rail devices Vcc ports respective to their rails

Use resistors at output to lower output voltage if desired
